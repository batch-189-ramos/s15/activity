let firsrNameForm = 'First Name:';
let	firstName = 'Rupert';
let lastNameForm = 'Last Name:';
let	lastName = 'Ramos';
let ageForm = 'Age:';
let age = 30;
let hobbiesName = 'Hobbies:';
let hobbies = ['Cooking', 'Playing Mobile Games', 'Drawing'];
let workAddress = 'Work Address';
let workAddressObj = {
	housenumber: 56,
	street: 'Balon Street',
	city: 'Caloocan City',
	state: 'Metro Manila',

};
let friendsNames = 'My Friends are:';
let friendsNamesList = ['Darien','Geine', 'Charofa', 'Cheddie', 'Erwin', 'Margie'];
let fullProfile = 'My Full Profile:';
let profileObj = {
	username: 'ruru_sensei',
	fullname: 'Rupert Ramos',
	age: 30,
	isActive: false,

};
let bestFriend = 'Darien Piedraverde';
let foundFrozen = 'Arctic Ocean';



// console logs
console.log('First Name: ' + firstName)
console.log('Last Name: ' + lastName)
console.log('Age: ' + age);
console.log(hobbiesName);
console.log(hobbies);
console.log(workAddress);
console.log(workAddressObj);
console.log('My full name is: ' + firstName + lastName);
console.log('My current age is: ' + age);
console.log(friendsNames);
console.log(friendsNamesList);
console.log(fullProfile);
console.log(profileObj);
console.log('My bestfriend is: ' + bestFriend);
console.log('I was found frozen: ' + foundFrozen);